#!/bin/bash
# this is designed to be run once per day

# where to put all this stuff.
BACKUP_DIR="$HOME/backups/database"

# Sun, Mon, Tue, etc
TODAY=`date +%a`

# get a list of all databases from MySQL
#
# -B: show results in batch mode (tab-delimited) instead of wrapped with ASCII + and |
# -e: execute the show databases command
#
# pipe the list through grep to get rid of results we don't want to try to backup (-e means "exclude")
#
# credentials need to be specified in ~/.my.cnf
DATABASES=`mysql -B -e "show databases" | grep -v -e Database -e information_schema -e performance_schema -e mysql`

#  loop over the list and backup each DB
for DB in $DATABASES
do
    # create a folder for this DB if it doesn't exist yet
    if [ ! -d "$BACKUP_DIR/$DB" ]; then
        mkdir -p "$BACKUP_DIR/$DB"
    fi

    mysqldump -x $DB | gzip -f > $BACKUP_DIR/$DB/$DB.$TODAY.sql.gz

done