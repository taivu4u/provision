alias sudo='sudo '
alias composer='php ~/bin/composer.phar'
alias create-site='php ~/bin/create-site.php'
alias v='cd /var/www/vhosts'
