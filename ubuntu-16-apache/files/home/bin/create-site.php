#!/usr/bin/php
<?php
/**
 * Gotta run this through sudo, since we're dealing with apache:
 *  sudo create-site www.example.com
 */
	if (count($argv) < 2 || posix_getuid() != 0) {
		die("\nUsage: sudo create-site www.example.com\n\n");
	}

	$domain = strtolower($argv[1]);

/**
 *	Strip off the leading www if it was specified, so we can get at
 *  the plain ol' "domain". This makes it easier to fill in the 
 *  bits in the server config where we redirect DOMAIN to www.DOMAIN
 */
	if (substr($domain, 0, 4) == 'www.') {
		$domain = substr($domain, 4);
	}

/**
 *  Ensure domain docroot isn't already present
 */
	$docroot = '/var/www/vhosts/www.' . $domain;
	if (file_exists($docroot)) {
		die("\nABORT: Site docroot already exists: " . $docroot . "\n\n");
	}

/**
 *  Ensure domain's web server config isn't already present
 */
	$config_filename = '/etc/apache2/sites-available/www.' . $domain . '.conf';
	if (file_exists($config_filename)) {
		die("\nABORT: Site config already exists: " . $config_filename . "\n\n");
	}

/**
 *  Create the web site config
 */
	$template = file_get_contents('/etc/apache2/sites-available/template.conf.txt');
	$new_config = str_replace('%DOMAIN', $domain, $template);
	file_put_contents($config_filename, $new_config);


/**
 *  Create the site's TLS directives (which are included in the HTTPS vhost)
 */
	$tls_filename = '/etc/apache2/sites-available/www.' . $domain . '.tls.txt';
	$template = file_get_contents('/etc/apache2/sites-available/template.tls.txt');
	$new_config = str_replace('%DOMAIN', $domain, $template);
	file_put_contents($tls_filename, $new_config);


/**
 *  Create the new site's project root. NOT pub/, since that will come down when we git clone a site.
 *  Then enable the HTTP version of the site. HTTPS we'll do manually later, since we'll need 
 *  to get the TLS certificate using certbot.
 */
	mkdir($docroot, 0755, true);
	exec('chown -R ' . get_current_user() . '. ' . $docroot);
	symlink('/etc/apache2/sites-available/www.' . $domain . '.conf', 
			'/etc/apache2/sites-enabled/www.' . $domain . '.conf');

/**
 *  Yay!
 */
	echo "\nNew Site has been created!\n\n";

	echo "  Hostname:      $domain\n";
	echo "  Document Root: $docroot\n";
	echo "  Vhost config:  $config_filename\n\n";

	echo "To complete installation, run the following command:\n\n";
	echo "  sudo service apache2 restart\n\n";