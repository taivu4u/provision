umask 0002

# Make sure global composer tools work
export PATH="$PATH:$HOME/bin"

if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

export HISTTIMEFORMAT="%d/%m/%y %T "