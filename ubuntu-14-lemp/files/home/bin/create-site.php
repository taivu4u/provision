#!/usr/bin/php
<?php
/**
 * Gotta run this through sudo, since we're dealing with nginx:
 *  sudo create-site example.com
 *  sudo create-site www.example.com
 */
	if (count($argv) < 2 || posix_getuid() != 0) {
		die("\nUsage: sudo create-site www.example.com\n\n");
	}

	$domain = strtolower($argv[1]);

/**
 *	Strip off the leading www if it was specified, so we can get at
 *  the plain ol' "domain". This makes it easier to fill in the 
 *  bits in teh server config where we redirect DOMAIN to www.DOMAIN
 */
	if (substr($domain, 0, 4) == 'www.') {
		$domain = trim($domain, 'www.');
	}

/**
 *  Ensure domain docroot isn't already present
 */
	$docroot = '/var/www/vhosts/www.' . $domain;
	if (file_exists($docroot)) {
		die("\nABORT: Site docroot already exists: " . $docroot . "\n\n");
	}

/**
 *  Ensure domain's nginx config isn't already present
 */
	$config_filename = '/etc/nginx/sites-available/www.' . $domain . '.conf';
	if (file_exists($config_filename)) {
		die("\nABORT: Site config already exists: " . $config_filename . "\n\n");
	}

/**
 *  Create the new site config file based on the config template
 */
	$template = file_get_contents('/etc/nginx/sites-available/template.conf.txt');
	$new_config = str_replace('%DOMAIN', $domain, $template);
	file_put_contents($config_filename, $new_config);


/**
 *  Create the new site's document root
 */
	mkdir($docroot . '/pub', 0755, true);
	exec('chown -R ' . get_current_user() . '. ' . $docroot);
	symlink('/etc/nginx/sites-available/www.' . $domain . '.conf', 
		'/etc/nginx/sites-enabled/www.' . $domain . '.conf');


/**
 *  Yay!
 */
	echo "\nNew Site has been created\n\n";

	echo "  Hostname:     $domain\n";
	echo "  Doc Root:     $docroot\n";
	echo "  Nginx config: $config_filename\n\n";

	echo "To complete installation, run the following command:\n\n";
	echo "  sudo service nginx restart\n\n";