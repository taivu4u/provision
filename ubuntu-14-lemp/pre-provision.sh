#!/bin/bash
apt-get install software-properties-common git python-passlib -y
apt-add-repository ppa:ansible/ansible -y
apt-get update
apt-get install ansible -y
apt-get autoremove -y