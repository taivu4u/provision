---
- hosts: localhost

  vars_prompt:
    - name: "system_hostname"
      prompt: "System hostname [fully-qualified, NOT apex. E.g., www.example.com, NOT example.com]"
      private: no

    - name: "system_username"
      prompt: "System username"
      private: no

    - name: "system_password"
      prompt: "System password"
      private: no
      encrypt: "sha512_crypt"
      salt_size: 7

    - name: "mysql_password"
      prompt: "MySQL password"
      private: no

    - name: "root_email"
      prompt: "Email address [system notifications]"
      private: no

  tasks:

###############################################################################
# Basic/general stuff
###############################################################################
  - name: Set system hostname
    hostname: name="{{system_hostname}}"

  - name: Install Basic packages
    register: basic_packages
    apt: pkg={{item}} state=installed update_cache=true cache_valid_time=3600
    with_items:
    - curl
    - fail2ban
    - htop
    - mailutils
    - mutt
    - ncdu
    - ntp
    - postfix
    - python-dev
    - python-mysqldb
    - python-pip
    - ufw
    - unattended-upgrades
    - vim
    - whois

  - name: Set timezone
    copy: 
      content='America/New_York'
      dest=/etc/timezone
      owner=root
      group=root
      mode=0644
      backup=yes
    notify:
      - Update Timezone

  - name: Install ps_mem
    when: basic_packages|success
    command: pip install ps_mem

  - name: Forward root email
    when: basic_packages|success
    lineinfile: "dest=/etc/aliases line='root: {{root_email}}'"

  - name: Reload Postfix aliases
    when: basic_packages|success
    command: newaliases

  - name: APT updates (auto-upgrades)
    when: basic_packages|success
    copy:
      src=files/unattended-upgrades/20auto-upgrades
      dest=/etc/apt/apt.conf.d/20auto-upgrades

  - name: APT updates (unattended-upgrades)
    when: basic_packages|success
    copy:
      src=files/unattended-upgrades/50unattended-upgrades
      dest=/etc/apt/apt.conf.d/50unattended-upgrades
      

###############################################################################
# Nginx
###############################################################################
  - name: Install Nginx
    apt: pkg=nginx state=installed update_cache=true
    register: nginx_installed

  - name: Disable Default Site
    when: nginx_installed|success
    file: dest=/etc/nginx/sites-enabled/default state=absent

  - name: Create System user
    register: system_user_exists
    when: nginx_installed|success
    user: 
      name={{system_username}}
      state=present
      groups=sudo,www-data 
      append=yes
      shell=/bin/bash
      generate_ssh_key=yes
      password={{system_password}}

  - name: Copy Nginx Files
    register: nginx_files_exist
    when: nginx_installed|success
    notify: Restart Nginx
    synchronize: 
      src=files/nginx/
      dest=/etc/nginx

  - name: Enable localhost site (for nginx_status)
    file: 
      src=/etc/nginx/sites-available/localhost.conf
      dest=/etc/nginx/sites-enabled/localhost.conf
      state=link

  - name: Ensure vhosts exists
    when: system_user_exists|success
    file: 
      path=/var/www/vhosts 
      state=directory 
      owner="{{system_username}}"
      group="{{system_username}}"

###############################################################################
# Miscellaneous
###############################################################################
  - name: Copy home dir files
    register: home_files_copied
    when: system_user_exists|success
    become_user: "{{system_username}}"
    copy: 
      src=files/home/
      dest=/home/{{system_username}}/
      owner={{system_username}}
      group={{system_username}}

  - name: SSH - no root access
    notify: Restart ssh
    lineinfile: 
      dest=/etc/ssh/sshd_config
      regexp="^PermitRootLogin"
      line="PermitRootLogin no"
      state=present

  # - name: SSH - allow password auth from specific IP
  # PasswordAuthentication no
  # Match address 192.168.0.1,10.0.0.1
  #   PasswordAuthentication yes

###############################################################################
# PHP
###############################################################################

  - name: Install PHP and Friends
    register: php_installed
    apt: pkg={{item}} state=installed update_cache=true
    with_items:
      - php5
      - php5-cli
      - php5-fpm
      - php5-xsl
      - php5-dev
      - php5-gd
      - php5-mcrypt
      - php5-mysql
      - php5-intl
      - php5-curl
    notify:
    - Start PHPFPM

  - name: PHP - disable cgi.fix_pathinfo in FPM
    when: php_installed|success
    lineinfile: 
      dest=/etc/php5/fpm/php.ini 
      regexp=^;cgi\.fix_pathinfo
      line="cgi.fix_pathinfo=0"
      state=present

  - name: PHP - Set post_max_size
    when: php_installed|success
    lineinfile: 
      dest=/etc/php5/fpm/php.ini 
      regexp=^post_max_size
      line="post_max_size = 10M"
      state=present

  - name: PHP - Set max_upload_filesize
    when: php_installed|success
    lineinfile: 
      dest=/etc/php5/fpm/php.ini 
      regexp=^max_upload_filesize
      line="max_upload_filesize = 10M"
      state=present

  - name: PHP - disable always_populate_raw_post_data in FPM
    when: php_installed|success
    lineinfile: 
      dest=/etc/php5/fpm/php.ini 
      regexp=^;always_populate_raw_post_data
      line="always_populate_raw_post_data = -1"
      state=present

  - name: PHP - disable always_populate_raw_post_data in CLI
    when: php_installed|success
    lineinfile: 
      dest=/etc/php5/cli/php.ini 
      regexp=^;always_populate_raw_post_data
      line="always_populate_raw_post_data = -1"
      state=present

###############################################################################
# PsySH
###############################################################################
  - name: Does psysh exist?
    register: psysh_phar
    when: php_installed|success and system_user_exists
    stat: 
      path=/home/{{system_username}}/bin/psysh

  - name: Download psysh
    when: not psysh_phar.stat.exists
    register: psysh_downloaded
    become_user: "{{system_username}}"
    get_url:
      url: https://git.io/psysh
      dest: /home/{{system_username}}/bin/psysh
      owner: "{{system_username}}"
      group: "{{system_username}}"
      mode: a+x


###############################################################################
# Composer
###############################################################################
  - name: Does composer exist?
    register: composer_phar
    when: php_installed|success and system_user_exists
    stat: 
      path=/home/{{system_username}}/bin/composer.phar

  - name: Download composer
    when: not composer_phar.stat.exists
    register: composer_downloaded
    become_user: "{{system_username}}"
    get_url:
      url: https://getcomposer.org/composer.phar
      dest: /home/{{system_username}}/bin/composer.phar
      owner: "{{system_username}}"
      group: "{{system_username}}"

###############################################################################
# Drupal Stuff
###############################################################################
  - name: Download Drush
    register: drush_download
    get_url: 
      url: http://files.drush.org/drush.phar 
      dest: /home/{{system_username}}/bin/drush
      owner: "{{system_username}}"
      group: "{{system_username}}"
      mode: 0755

  - name: Download Drupal Console
    register: console_download
    get_url:
      url: https://drupalconsole.com/installer
      dest: /home/{{system_username}}/bin/drupal
      owner: "{{system_username}}"
      group: "{{system_username}}"
      mode: 0755


###############################################################################
# MySQL
###############################################################################
  - name: Install MySQL
    register: mysql_installed
    apt: 
      pkg=mysql-server 
      state=installed
      update_cache=true

  - name: Enable InnoDB file-per-table
    when: mysql_installed|success
    lineinfile: 
      dest=/etc/mysql/my.cnf
      line="innodb_file_per_table=1"
    
  - name: Create MySQL user
    register: mysql_user
    when: mysql_installed|success
    mysql_user:
      host: localhost
      name: "{{system_username}}"
      password: "{{mysql_password}}"
      priv: "*.*:ALL,GRANT"
      check_implicit_admin: yes
      state: present

  - name: Does .my.cnf exist?
    register: mycnf_exists
    when: mysql_installed|success and system_user_exists
    stat: path=/home/{{system_username}}/.my.cnf

  - name: Add custom .my.cnf
    when: not mycnf_exists.stat.exists
    blockinfile:
      dest: /home/{{system_username}}/.my.cnf
      create: yes
      marker: ""
      owner: "{{system_username}}"
      group: "{{system_username}}"
      mode: 0600
      block: |
        [mysql]
        user={{system_username}}
        password={{mysql_password}}
        [mysqldump]
        user={{system_username}}
        password={{mysql_password}}

  - name: mysql backup
    cron: 
      name="mysql backup"
      user={{system_username}}
      special_time=daily
      job="/bin/bash ~/cron/database-backup.sh"

###############################################################################
# Certbot (LetsEncrypt)
###############################################################################
  - name: Does certbot exist?
    register: certbot_file
    stat: 
      path=/usr/local/bin/certbot-auto

  - name: Download certbot
    when: not certbot_file.stat.exists
    register: certbot_downloaded
    get_url:
      url: https://dl.eff.org/certbot-auto
      dest: /usr/local/bin/certbot-auto
      mode: 0755

  - name: Setup Certbot
    when: certbot_downloaded|success
    register: certbot_setup
    command: /usr/local/bin/certbot-auto renew --non-interactive

  - name: Create certbot cron
    when: certbot_setup|success
    cron: 
      name="certbot daily renewal"
      user="{{system_username}}"
      minute="42"
      hour="4"
      job="sudo /usr/local/bin/certbot-auto renew --quiet --no-self-upgrade && sudo service nginx reload"

  - name: Sudoer file for certbot
    when: certbot_setup|success
    blockinfile:
      dest: /etc/sudoers.d/certbot
      create: yes
      marker: ""
      mode: 0440
      block: |
        Cmnd_Alias NGINX_RELOAD  = /usr/sbin/service nginx reload
        Cmnd_Alias CERTBOT       = /home/cyberwoven/.local/share/letsencrypt/bin/letsencrypt
        {{system_username}} ALL=NOPASSWD: NGINX_RELOAD, CERTBOT

###############################################################################
# Firewall
###############################################################################
  - name: Firewall (allow SSH)
    ufw: rule=allow port=ssh

  - name: Firewall (allow HTTP)
    ufw: rule=allow port=http

  - name: Firewall (allow HTTPS)
    ufw: rule=allow port=https

  - name: Firewall (enable)
    ufw: state=enabled

###############################################################################
# Handlers
###############################################################################
  handlers:
  - name: Start Nginx
    service: name=nginx state=started

  - name: Restart Nginx
    service: name=nginx state=restarted

  - name: Start PHPFPM
    service: name=php5-fpm state=started
    
  - name: Restart ssh
    service: name=ssh state=restarted

  - name: Update Timezone
    command: dpkg-reconfigure --frontend noninteractive tzdata