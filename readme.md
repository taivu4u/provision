# Provisioning

This project is designed to be used as a simple way to ensure that our servers are all provisioned the same way. In order to make this work, the target server must meet the following requirements:

  - Installed with Ubuntu 16+

### Usage
Once the target server has been intially setup, the following commands (run as root) will get it provisioned:

```sh
cd /root
apt-get install git -y
git clone https://bitbucket.org/cyberwoven/provision
cd provision/ubuntu-16-apache/
bash pre-provision.sh
ansible-playbook playbook.yml
```
